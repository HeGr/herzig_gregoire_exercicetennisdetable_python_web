-- Cette requête permet de chercher tout ce qui contient le caractère entre les <'% ... %' >
    -- SELECT * FROM `...` WHERE `...` LIKE '%...%'
SELECT * FROM `t_entraineur` WHERE `Prenom_Entraineur` LIKE '%i%'
-- RESULTAT : Gilles - Julien - Grégoire

-- Cette requête permet de rechercher précisemment une chaîne de caractère spécifiée entre les < '' >.
    -- SELECT * FROM `...` WHERE `...` LIKE '...'
SELECT * FROM `t_entraineur` WHERE `Prenom_Entraineur`LIKE 'Gilles'
-- Result : Gilles

--Requête permettant de rechercher soit une chaîne de caractère spécifique (nom en entier) soit un nombre
    -- SELECT * FROM `...` WHERE `...` = ...
SELECT * FROM `t_entraineur`where `id_entraineur` = 3
SELECT * FROM `t_entraineur`where `Nom_Entraineur` = 'Ming'

--Requete différent de
SELECT * FROM `t_entraineur`where `Prenom_Entraineur` != 'Grégoire'

-- Requête "pas comme"
SELECT * FROM `t_entraineur` WHERE `Nom_Entraineur` NOT LIKE 'i' -- affiche tous les entraîneurs
SELECT * FROM `t_entraineur` WHERE `Nom_Entraineur` NOT LIKE '%i%' -- affiche uniquement les noms ne contenant pas de 'i'

-- Requête IN() qui va ressortir l'id du type d'exercice dans Débutant et Expert
SELECT id_TypeExercie FROM `t_typeexercice` WHERE `Type_TypeExercice` IN('Débutant','Expert')
    -- le NOT IN() va donner comme résultat les ids qui ne sont pas égaux à Débutant et Expert
SELECT id_TypeExercie FROM `t_typeexercice` WHERE `Type_TypeExercice` NOT IN('Débutant','Expert')
--Requête BETWEEN et NOT BETWEEN
    --SELECT ... FROM `...`WHERE `...` BETWEEN `...` AND `...`
    --SELECT ... FROM `...` WHERE `...` NOT BETWEEN `...` AND `...`
SELECT * FROM `t_typeexercice` WHERE `Type_TypeExercice` BETWEEN 'Echauffement' AND 'Expert'
--RESULTAT : 1 - Echauffement, 5 - Expert
SELECT * FROM `t_typeexercice` WHERE `Type_TypeExercice` NOT BETWEEN 'Echauffement' AND 'Expert'
--RESULTAT : 3 - Débutant, 4 - Avancé

--Requêtes REGEXP , REGEXP ^...$, IS (NOT) NULL

--REGEXP ^........$ veut dire que l'on cherche un mot contenant exactement 8 caractères
SELECT * FROM `t_entraineur` WHERE `Prenom_Entraineur` ^........$

--Requête =" signifie strictement équivalant et !=" l'inverse. c'est comme un LIKE mais pour le format numéraire
SELECT *FROM `t_typeexercice` WHERE `Type_TypeExercice` =" '3'
--requete plus complexe :

SELECT NumeroJS_Entraineur, nom_entraineur FROM t_entraineur as t1 
INNER JOIN t_personne as t2 on t1.NumeroJS_Entraineur = 1102352

--Resultat : NumeroJS_Entraineur
--nom_entraineur
--1102352
--Herzig
--1102352
--Herzig
--1102352
--Herzig

SELECT Nom_Exercice, id_Exercice FROM t_exercice as T1
INNER JOIN t_entraineur as T2 on T2.Nom_Entraineur = 'Herzig'

--RESULTAT :
-- NOM_EXERCICE : HUIT - Démarrage du en top frotté du CD
-- id_Exercice : 1- 3