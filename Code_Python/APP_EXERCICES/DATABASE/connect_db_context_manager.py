#connect_db_context_manager.py
#GH 2020.04.21 Classe pour se connecter à la base de données
#
#Le coeur du dsystème pour la connexion à la BD
import pymysql
from Code_Python.APP_EXERCICES.DATABASE.erreurs import *
# Petits messages "flash", échanges entre Python et Jinja dans une page en HTML
from flask import flash
class MaBaseDeDonnee():
    # Quand on instancie la classe, il interprète le code __init__
    def __init__(self):
        self.host = '127.0.0.1'
        self.user = 'root'
        self.password = ''
        self.db = "herzig_gregoire_extennistable_bd_104"

        self.connexion_bd = None
        try:
            # Connexion à la base de donnée
            # Attention au password !!!!
            self.connexion_bd = pymysql.connect(host=self.host,
                                                user=self.user,
                                                password=self.password,
                                                db=self.db,
                                                cursorclass=pymysql.cursors.DictCursor,
                                                autocommit=False)
            print(" Avec CM BD CONNECTEE, TOUT va BIEN !! Dans le constructer")
            print("self.con....", dir(self.connexion_bd),"type of self.con : ", type(self.connexion_bd))
            flash('Connecté avec succès', 'success')
        # il y a un problème avec la BD (non connectée, nom erronné, etc)
        #
        except (Exception,
                ConnectionRefusedError,
                pymysql.err.OperationalError,
                pymysql.err.DatabaseError) as erreur:
            # GH 2020.04.21 SI LA BD N'EST PAS CONNECTEE, ON ENVOIE AU TERMINAL DES MESSAGES POUR RASSURER L'UTILISATEUR
            # Petits messages "flash", échange entre Python et Jinja dans une page en HTML
            flash(f"Flash...BD NON CONNECTEE. Erreur : {erreur.arg[1]}", "danger")
            #raise, permet de "lever" un exception et de personnaliser la page d'erreur
            #voir le ficher "run_mon_app.py"
            print("erreur...MaBaseDeDonnee.__init__ ",erreur.args[1])
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")
        print("Avec CM BD  INIT !! ")

    # Après __init__ il passe à __enter__, c'est là qu'il faut surveilleur le bon déroulement
    # des actions. en cas de problème il ne va pas dans la méthode __exit
    def __enter__(self):
        return self


    # Méthode de sorte de la classe, c'est là que se trouve tout ce qui doit être fermé
    # Si un problème (une Excepetion est levée avant (__init__ ou __enter__) cette méthode
    # n'est pas interpretée
    def __exit__(self, exc_type, exc_val, traceback):
        #La valeur des paramètres est "None" si tout s'est bien déroulé
        print("exc_val ",exc_val)
        """
            Si la sortie se passe bien ==> commit. Si exception ==> rollback
            
            Tous les paramètres sont de valeur "None" s'il n'y a pas eu d'EXCEPTION
            En Python "None" est défini par la valeur "False"
        """
        if exc_val is None:
            print("commit !! Dans le destructeur " )
            self.connexion_bd.commit()
        else:
            print("rollbakc !! Dans le destructeur")
            self.connexion_bd.rollback()

        # Fermeture de la connexion à la base de donnée
        self.connexion_bd.close()
        print("La BD est FERMEE !! Dans le destructeur")

    # OM 2020.04.10 Cette méthode est définie pour utiliser les "context manager"
    # Une fois l'interprétation de cette méthode terminée
    # le destructeur "__exit__" sera automatiquement interprété.
    # ainsi après avoir éxécuté la requête MySql on va faire un commit (enregistrer les modifications)
    # s'il n'y a pas erreur ou un rollback (retour en arrière) en cas d'erreur
    # et finalement fermer la connexion à la BD.
    def mabd_execute(self, sql, params=None):
        print("execute",sql," params", params)
        return self.connexion_bd.cursor().execute(sql, params or ())

    # OM 2020.04.10 Cette méthode est définie pour utiliser les "context manager"
    def mabd_fetchall(self):
        return self.connexion_bd.cursor().fetchall()