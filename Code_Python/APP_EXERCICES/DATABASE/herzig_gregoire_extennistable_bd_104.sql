-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 21 avr. 2020 à 15:55
-- Version du serveur :  10.4.6-MariaDB
-- Version de PHP :  7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `herzig_gregoire_extennistable_bd_104`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_avoirexercice`
--

CREATE TABLE `t_avoirexercice` (
  `id_AvoirExercice` int(11) NOT NULL COMMENT 'clé primaire de la table',
  `FK_Exercice` int(11) NOT NULL COMMENT 'clé étrangère de la table exercice',
  `FK_Entraineur` int(11) NOT NULL COMMENT 'clé étrangère de la table entraineur',
  `Date_AvoirExercice` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'date de la création de l''exercice ou de son ajout à la DB'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_avoirlicencejs`
--

CREATE TABLE `t_avoirlicencejs` (
  `id_AvoirLicenceJS` int(11) NOT NULL COMMENT 'clé primaire de la table',
  `FK_Entraineur` int(11) NOT NULL COMMENT 'clé étrangère de la table entraineur',
  `FK_Personne` int(11) NOT NULL COMMENT 'clé étrangère de la table personne',
  `Date_AvoirLicenceJS` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'date d''ajout de la licence à la personne'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_avoirlicencejs`
--

INSERT INTO `t_avoirlicencejs` (`id_AvoirLicenceJS`, `FK_Entraineur`, `FK_Personne`, `Date_AvoirLicenceJS`) VALUES
(1, 1, 1, '2019-12-01 23:00:00'),
(8, 4, 5, '0000-00-00 00:00:00'),
(9, 3, 6, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `t_avoirmateriel`
--

CREATE TABLE `t_avoirmateriel` (
  `id_AvoirMateriel` int(11) NOT NULL COMMENT 'clé primaire de la table',
  `FK_Exercice` int(11) NOT NULL COMMENT 'clé étrangère de la table exercice',
  `FK_Materiel` int(11) NOT NULL COMMENT 'clé étrangère de la table matériel'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_avoirtype`
--

CREATE TABLE `t_avoirtype` (
  `id_avoirtype` int(11) NOT NULL COMMENT 'clé primaire de la table',
  `Fk_type` int(11) NOT NULL COMMENT 'clé étrangère de la table type',
  `Fk_exercice` int(11) NOT NULL COMMENT 'clé étrangère de la table exercice'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_avoirtype`
--

INSERT INTO `t_avoirtype` (`id_avoirtype`, `Fk_type`, `Fk_exercice`) VALUES
(1, 3, 1),
(2, 4, 3);

-- --------------------------------------------------------

--
-- Structure de la table `t_entraineur`
--

CREATE TABLE `t_entraineur` (
  `id_Entraineur` int(11) NOT NULL COMMENT 'clé primaire de la table',
  `Nom_Entraineur` varchar(49) NOT NULL COMMENT 'Nom entraineur = Nom de la personne dans T_Personne',
  `Prenom_Entraineur` varchar(21) NOT NULL COMMENT 'Prénom entraineur = Prénom de la personne dans T_Personne',
  `NumeroJS_Entraineur` varchar(15) DEFAULT NULL COMMENT 'Numéro de la licence JS (on autorise la valeur NULL si le N° n''est pas encore disponible)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_entraineur`
--

INSERT INTO `t_entraineur` (`id_Entraineur`, `Nom_Entraineur`, `Prenom_Entraineur`, `NumeroJS_Entraineur`) VALUES
(1, 'Herzig', 'Grégoire', '1102352'),
(3, 'Vaucher', 'Gilles', ''),
(4, 'Ming', 'Julien', ''),
(9, 'Testeur', 'Test', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `t_exercice`
--

CREATE TABLE `t_exercice` (
  `id_Exercice` int(11) NOT NULL COMMENT 'clé primaire de la table',
  `Visuel_Exercice` varchar(255) NOT NULL COMMENT 'on insère ici un shéma ou une vidéo de l''exercice',
  `Nom_Exercice` text NOT NULL COMMENT 'Nom de l''exercice',
  `Description_Exercice` text NOT NULL COMMENT 'Description de l''exercice (étapes/consignes...)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_exercice`
--

INSERT INTO `t_exercice` (`id_Exercice`, `Visuel_Exercice`, `Nom_Exercice`, `Description_Exercice`) VALUES
(1, '', 'Contre-attaque CD', 'LE joureur A distribue des balles dans le CD de joueur B en CD. jB contre-attaque dans le CD de jA'),
(2, '', 'HUIT', 'Joueur A fait de la contre-attaque en diagonale.Joueur B fait de la contre-attaque en ligne droite'),
(3, '', 'Démarrage en top frotté du CD', 'Joueur A fait envoie une balle coupée (ou plate basse) dans le CD du Joueur B.Joueur B descend bien sur jambe droite et frotte la balle du bas vers le haut avec un transfertde poids vers  la jambe gauche. Sa balle doit aller dans le CD du joueur A'),
(4, '', 'Démarrage revers', 'Même principe que démarrage en top frotté du CD mais en RV');

-- --------------------------------------------------------

--
-- Structure de la table `t_materiel`
--

CREATE TABLE `t_materiel` (
  `id_Materiel` int(11) NOT NULL COMMENT 'clé primaire de la table',
  `Matos_Materiel` varchar(42) NOT NULL COMMENT 'type de matériel '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_materiel`
--

INSERT INTO `t_materiel` (`id_Materiel`, `Matos_Materiel`) VALUES
(1, 'Petits cônes'),
(2, 'Cônes'),
(3, 'Feuilles A4 (ou autres)'),
(4, 'Balles différentes (tailles, matières, cou'),
(5, 'Elastiques'),
(6, 'Séparations'),
(7, 'Cordes'),
(8, 'Panier de balle'),
(9, 'Filets du panier de balles'),
(10, 'Ramasseur de balles');

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE `t_personne` (
  `id_personne` int(11) NOT NULL COMMENT 'clé primaire de la table',
  `nom_personne` varchar(42) NOT NULL COMMENT 'nom de la personne = nom de l''entraineur',
  `prenom_personne` varchar(21) NOT NULL COMMENT 'prénom de la personne = prénom de l''entraineur',
  `dateNaissance_personne` date NOT NULL COMMENT 'date de naissance (AAAA-MM-JJ)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_personne`
--

INSERT INTO `t_personne` (`id_personne`, `nom_personne`, `prenom_personne`, `dateNaissance_personne`) VALUES
(1, 'Herzig', 'Grégoire', '1997-03-29'),
(5, 'Ming', 'Julien', '0000-00-00'),
(6, 'Vaucher', 'Gilles', '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `t_typeexercice`
--

CREATE TABLE `t_typeexercice` (
  `id_TypeExercice` int(11) NOT NULL COMMENT 'Clé primaire de la table type exercice',
  `Type_TypeExercice` text NOT NULL COMMENT 'C''est ici que l''on va dire quel type d''exercice on a (p.ex : débutant, avancé, ...)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_typeexercice`
--

INSERT INTO `t_typeexercice` (`id_TypeExercice`, `Type_TypeExercice`) VALUES
(3, 'Débutant'),
(4, 'Avancé'),
(5, 'Légendes');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `t_avoirexercice`
--
ALTER TABLE `t_avoirexercice`
  ADD PRIMARY KEY (`id_AvoirExercice`),
  ADD KEY `FK_Exercice` (`FK_Exercice`),
  ADD KEY `FK_TypeExercice` (`FK_Entraineur`);

--
-- Index pour la table `t_avoirlicencejs`
--
ALTER TABLE `t_avoirlicencejs`
  ADD PRIMARY KEY (`id_AvoirLicenceJS`),
  ADD KEY `FK_Entraineur` (`FK_Entraineur`),
  ADD KEY `FK_Personne` (`FK_Personne`);

--
-- Index pour la table `t_avoirmateriel`
--
ALTER TABLE `t_avoirmateriel`
  ADD PRIMARY KEY (`id_AvoirMateriel`),
  ADD KEY `FK_Exercice` (`FK_Exercice`),
  ADD KEY `FK_Materiel` (`FK_Materiel`);

--
-- Index pour la table `t_avoirtype`
--
ALTER TABLE `t_avoirtype`
  ADD PRIMARY KEY (`id_avoirtype`),
  ADD KEY `Fk_type` (`Fk_type`),
  ADD KEY `Fk_exercice` (`Fk_exercice`);

--
-- Index pour la table `t_entraineur`
--
ALTER TABLE `t_entraineur`
  ADD PRIMARY KEY (`id_Entraineur`);

--
-- Index pour la table `t_exercice`
--
ALTER TABLE `t_exercice`
  ADD PRIMARY KEY (`id_Exercice`);

--
-- Index pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
  ADD PRIMARY KEY (`id_Materiel`);

--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`id_personne`);

--
-- Index pour la table `t_typeexercice`
--
ALTER TABLE `t_typeexercice`
  ADD PRIMARY KEY (`id_TypeExercice`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `t_avoirexercice`
--
ALTER TABLE `t_avoirexercice`
  MODIFY `id_AvoirExercice` int(11) NOT NULL AUTO_INCREMENT COMMENT 'clé primaire de la table';

--
-- AUTO_INCREMENT pour la table `t_avoirlicencejs`
--
ALTER TABLE `t_avoirlicencejs`
  MODIFY `id_AvoirLicenceJS` int(11) NOT NULL AUTO_INCREMENT COMMENT 'clé primaire de la table', AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `t_avoirmateriel`
--
ALTER TABLE `t_avoirmateriel`
  MODIFY `id_AvoirMateriel` int(11) NOT NULL AUTO_INCREMENT COMMENT 'clé primaire de la table';

--
-- AUTO_INCREMENT pour la table `t_avoirtype`
--
ALTER TABLE `t_avoirtype`
  MODIFY `id_avoirtype` int(11) NOT NULL AUTO_INCREMENT COMMENT 'clé primaire de la table', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `t_entraineur`
--
ALTER TABLE `t_entraineur`
  MODIFY `id_Entraineur` int(11) NOT NULL AUTO_INCREMENT COMMENT 'clé primaire de la table', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `t_exercice`
--
ALTER TABLE `t_exercice`
  MODIFY `id_Exercice` int(11) NOT NULL AUTO_INCREMENT COMMENT 'clé primaire de la table', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
  MODIFY `id_Materiel` int(11) NOT NULL AUTO_INCREMENT COMMENT 'clé primaire de la table', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT COMMENT 'clé primaire de la table', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `t_typeexercice`
--
ALTER TABLE `t_typeexercice`
  MODIFY `id_TypeExercice` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clé primaire de la table type exercice', AUTO_INCREMENT=9;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_avoirexercice`
--
ALTER TABLE `t_avoirexercice`
  ADD CONSTRAINT `t_avoirexercice_ibfk_1` FOREIGN KEY (`FK_Entraineur`) REFERENCES `t_entraineur` (`id_Entraineur`),
  ADD CONSTRAINT `t_avoirexercice_ibfk_2` FOREIGN KEY (`FK_Exercice`) REFERENCES `t_exercice` (`id_Exercice`);

--
-- Contraintes pour la table `t_avoirlicencejs`
--
ALTER TABLE `t_avoirlicencejs`
  ADD CONSTRAINT `t_avoirlicencejs_ibfk_1` FOREIGN KEY (`FK_Entraineur`) REFERENCES `t_entraineur` (`id_Entraineur`),
  ADD CONSTRAINT `t_avoirlicencejs_ibfk_2` FOREIGN KEY (`FK_Personne`) REFERENCES `t_personne` (`id_personne`);

--
-- Contraintes pour la table `t_avoirmateriel`
--
ALTER TABLE `t_avoirmateriel`
  ADD CONSTRAINT `t_avoirmateriel_ibfk_1` FOREIGN KEY (`FK_Exercice`) REFERENCES `t_exercice` (`id_Exercice`),
  ADD CONSTRAINT `t_avoirmateriel_ibfk_2` FOREIGN KEY (`FK_Materiel`) REFERENCES `t_materiel` (`id_Materiel`);

--
-- Contraintes pour la table `t_avoirtype`
--
ALTER TABLE `t_avoirtype`
  ADD CONSTRAINT `t_avoirtype_ibfk_1` FOREIGN KEY (`Fk_exercice`) REFERENCES `t_exercice` (`id_Exercice`),
  ADD CONSTRAINT `t_avoirtype_ibfk_2` FOREIGN KEY (`Fk_type`) REFERENCES `t_typeexercice` (`id_TypeExercice`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
