#data_gestion_type.py
# GH 2020.04.21 Permet de gérer les CRUD des données de la table t_typeexercice
from flask import flash
from Code_Python.APP_EXERCICES.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from Code_Python.APP_EXERCICES.DATABASE.erreurs import *

class GestionType():
    def __init__(self):
        try:
            # DEBUG bon marché : afficher un message dans la console
            print("dans le try de gestion type")
            # la connexion à la base de donnée est-elle active ?
            # renvoie une erreur si la connexion est perdue
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion Type ...terrible erreur, il faut connecter une base de donnée", "danger")
            # DEBUG bon marché : aafficher un message dans la console
            print(f"Exception grave CLasse constructeur GestionType {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionType")

    def type_afficher_data(self):
        try:
            # selection des champs à afficher
            # permet de lever une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # on sélectionne les champs à afficher et on affiche dans l'ordre croissant (ascending) par rapport à l'id_typeexercice
            strsql_type_afficher = """SELECT id_TypeExercice, Type_TypeExercice FROM t_typeexercice ORDER BY id_TypeExercice ASC"""
            # du faite de l'utilisation des context manager, on accède au curseur avec with
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                #envoi de la commande MySql
                mc_afficher.execute(strsql_type_afficher)
                #Récupère les données de la requête
                data_type = mc_afficher.fetchall()
                #affichage dans la console
                print("data type ",data_type, "type ", type(data_type))
                # retourne les données du SELECT
                return data_type
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise  MaBdErreurPyMySl(f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def add_type_data(self, valeurs_insertion_dictionnaire):
        try:
            strsql_insert_type = """INSERT  INTO t_typeexercice (id_TypeExercice, Type_TypeExercice) VALUES (NULL, %(value_intitule_type)s)"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_type,valeurs_insertion_dictionnaire)

        except  pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

    def edit_type_data(self,valeur_id_dictionnaire):
        try:
            print("BUG",valeur_id_dictionnaire)
            #Commande sql pour afficher le type sélectionnée dans le tableau dans le formulaire HTML
            str_sql_id_type = "SELECT id_TypeExercice, Type_TypeExercice FROM t_typeexercice WHERE id_TypeExercice = %(value_id_type)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_type, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire", data_one)
                    return data_one
        except Exception as erreur:
            # GH 2020.04.21 MEssage en cas d'échec du bon dérouelement des commandes ci-dessus
            print(f"Problème edit_type_data Data Gestion Type numéro de l'erreur : {erreur}", "danger")
            # On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception...Problème edit_type_data d'un type Data Gestion Type {erreur}")

    def update_type_data(self, valeur_update_dictionnaire):
        try:
            print("DEBUG",valeur_update_dictionnaire)
            # GH 2020.04.21 Commande MySql pour la modification de la valeur tapée au clavier dans le champ "nameEditTypeHTML" du form HTML "type_edit.html"
            # le %s permet d'éviter les injection sql simples
            # <td><input type = "text" name "nameEditTypeHTML" value="{{ row.Type_TypeExercice"/></td>

            # commande sql pour afficher le type sélectionné dans le tableau dans le formulaire HTML
            str_sql_update_id_type = "UPDATE t_typeexercice SET Type_TypeExercice = %(value_name_type)s WHERE id_TypeExercice = %(value_id_type)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_id_type, valeur_update_dictionnaire)
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus
            print(f"Problème update_type_data Data Gestion Type numéro de l'erreur : {erreur}")

            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # 2 manière de communiquer une erreur causée par l'insertion d'une valeur à double
                flash("Doublo !! Introduire une valeur différente")
                # Message en cas d'échec du bon déroulement des commandes ci-dessus
                print(f"Problème update_type_data Data Gestion Type numéro de l'erreur : {erreur}")

                raise Exception("Raise exception...Problème update_type_data d'un type DataGestionType {erreur}")

    def delete_select_type_data(self, valeur_delete_dictionnaire):
        try:
            print("valeur dico BUUUUUUUGGGGGGGEEEEEEERRRRR ",valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleGenreHTML" du form HTML "GenresEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleGenreHTML" value="{{ row.intitule_genre }}"/></td>

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le genre sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_type = "SELECT id_TypeExercice, Type_TypeExercice FROM t_typeexercice WHERE id_TypeExercice = %(value_id_type)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une gméthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_type, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_select_type_data Gestions Type numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_type_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_type_data d\'un type Data Gestions Genres {erreur}")

    def delete_type_data(self, valeur_delete_dictionnaire):
        try:
            print("valeur dico DEBUUUUGGGGGGGGEEEEEEEEEEEERRRRRRR ",valeur_delete_dictionnaire)
            # GH 2019.04.21 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "type_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleTypeHTML" value="{{ row.intitule_ytpe }}"/></td>
            str_sql_delete_intituletype = "DELETE FROM t_typeexercice WHERE id_TypeExercice = %(value_id_type)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_intituletype, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...",data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_type data Data Gestions Type numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Type numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # OM 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un type qui est associé à un exercice dans la table intermédiaire "t_avoirtype"
                # il y a une contrainte sur les FK de la table intermédiaire "t_avoirtype"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce type est associé à des exercices dans la t_avoirtype !!! : {erreur}", "danger")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !!! Ce type est associé à des exercice dans la t_avoirtype !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")
