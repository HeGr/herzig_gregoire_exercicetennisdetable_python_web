#routes_gestion_exercice.py
# GH 2020.04.21 Gestion des "routes" FLASK pour les exercices

import pymysql
from Code_Python import *
from flask import render_template, flash, request, redirect, url_for
from Code_Python.APP_EXERCICES import obj_mon_application
from Code_Python.APP_EXERCICES.EXERCICE.data_gestion_exercice import GestionExercice
from Code_Python.APP_EXERCICES.DATABASE.connect_db_context_manager import MaBaseDeDonnee, MonErreur, \
    MaBdErreurConnexion, msg_erreurs
import re


# GH 2020.04.21 Afficher un avertissement sympa...mais contraignant
# Pour le tester http://127.0.0.1:1234/avertissement_sympa_pour_geeks
@obj_mon_application.route("/avertissement_sympa_pour_geeks")
def avertissement_sympa_pour_geeks():
    # envoie la page HTML au serveur
    return render_template("exercice/AVERTISSEMENT_SYMPA_POUR_LES_GEEKS_exercice.html")




# Afficher les exercices
# Pour tester http://127.0.0.1:1234/exercice_afficher
@obj_mon_application.route("/exercice_afficher")
def exercice_afficher():
    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de données par des champs
    # du formulaire HTML
    if request.method == "GET":
        try:
            # objet contenant toutes les méthodes CRUD des données
            obj_actions_exercice = GestionExercice()
            # récupère les données grâce à une requête MySql définie dans GestionExercice()
            # Fichier data_gestion_exercice.py
            data_exercice = obj_actions_exercice.exercice_afficher_data()
            # DEBUG bon marché : pour afficher un message dans la console
            print(" data exercice", data_exercice," type ",type(data_exercice))

            # la ligne suivante permet de donner un sentiment rassurant à l'utilisateur
            flash("Données exercice affichées !!", "Success")
        except Exception as erreur:
            print(f"RGF Erreur générale")
            # On dérive "Exception" par le "@obj_mon_application_errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}")
    # Envoie la page au serveur
    return render_template("exercice/exercice_afficher.html", data=data_exercice)
# Ajouter les exercices
# Pour tester http://127.0.0.1:1234/exercice_add
@obj_mon_application.route("/exercice_add", methods=['GET','POST'])
def exercice_add():
    # on vérifie si les données sont un affichage ou un envoi par un formulaire HTML
    if request.method=="POST":
        try:
            obj_actions_exercice = GestionExercice()

            # Récupération des champs
            name_exercice = request.form['name_exercice_html']
            visuel_exercice = request.form['visuel_exercice_html']
            description_exercice = request.form['visuel_description_html']

            # pas de nécessité d'insérer une regex pour les exercices
            valeurs_insertion_dictionnaire = {"value_visuel_exercice": visuel_exercice,
                                              "value_nom_exercice": name_exercice,
                                              "value_description_exercice": description_exercice}
            obj_actions_exercice.add_exercice_data(valeurs_insertion_dictionnaire)

            # rassurer l'utilisateur
            flash(f'Données insérées !!!', 'Success')
            print(f"Données insérées !!!")
            # on va interpréter la route "exercice_afficher" car l'utilisateur doit voir
            # le nouvel exercice qu'il vient d'insérer
            return redirect(url_for('exercice_afficher'))
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}")
            raise MonErreur(f"Autre erreur")
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGT Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    return render_template("exercice/exercice_add.html")

# Editer un exercice
@obj_mon_application.route("/exercice_edit", methods=['GET','POST'])
def exercice_edit():
    # les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton edit dans e formulaire "exercice_afficher.html"
    if request.method=='GET':
        try:
            # Récupérer la valeur de l'id_exercice
            id_exercice_edit = request.values['id_exercie_edit_html']
            print("id_exercice_edit", id_exercice_edit)

            #Dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_exercice": id_exercice_edit}

            obj_actions_exercice = GestionExercice()
            data_id_exercice = obj_actions_exercice.edit_exercice_data(valeur_select_dictionnaire)
            print("dataIDExercice : ", data_id_exercice, " type : ", type(data_id_exercice))

            #rassurer l'utilisateur
            flash(f'Editer un exercice')
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("exercice/exercice_edit.html", data=data_id_exercice)

# UPDATE un exercice (après edit)
@obj_mon_application.route("/exercice_update", methods=['GET','POST'])
def exercice_update():
    # debug bon marché pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))

    if request.method =='POST':
        try:
            print("request values : ",request.values)
             # Récupère la valeur de l'id_Exercice à updater
            id_exercice_edit = request.values['id_exercice_edit_html']

            # récupère le contenu des champs
            name_exercice = request.values['name_edit_nom_exercice']
            visuel_exercice = request.values['name_edit_visuel_exercice']
            description_exercice = request.values['name_edit_description_exercice']

            # création d'une liste avec le contenu des champs du form "edit_exerice.html
            valeur_edit_list = [{'id_Exercice': id_exercice_edit,
                                 'visuel_exercice': visuel_exercice,
                                 'description_exercice': description_exercice,
                                 'nom_exercice': name_exercice}]

            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF]*['\\- ]?[A-Za-z\u00C0-\u00FF]+$",name_exercice):
                flash('Une entrée incorrecte...!!! Pas de chiffres, pas de caractères spéciaux, d\'espace à double, de double apostrophe'
                      'de double trait d\'union et ne doit pas être vide !!!', 'Danger')
                # On doit afficher à nouveau le formulaire "genres_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "genres_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_type': 13, 'intitule_type': 'philosophique'}]
                valeur_edit_list = [{'id_Exercice': id_exercice_edit,
                                     'visuel_exercice': visuel_exercice,
                                     'description_exercice': description_exercice,
                                     'nom_exercice': name_exercice}]

                # Debug bon marché
                # Pour afficher le contenu et le type de valeurs passées au formulaire "type_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template("exercice/exercice_edit.html", data=valeur_edit_list)
            else:
                valeur_update_dictionnaire ={'id_Exercice': id_exercice_edit,
                                            'visuel_exercice': visuel_exercice,
                                            'description_exercice': description_exercice,
                                            'nom_exercice': name_exercice}
                obj_actions_exercice = GestionExercice()
                data_id_exercice = obj_actions_exercice.update_exercice_data(valeur_update_dictionnaire)
                print("DataIDExercice", data_id_exercice, " type : ", type(data_id_exercice))
                flash('Editer un exercice')
                return redirect(url_for('exercice_afficher'))
        except ( Exception,
                 pymysql.err.OperationalError,
                 pymysql.ProgrammingError,
                 pymysql.InternalError,
                 pymysql.IntegrityError,
                 TypeError) as erreur:

            print(erreur.args)
            flash(f"problème type update {erreur.args[0]}")
            # en cas de problème, mais surtout en cas de non respect
            # des règles REGEX dans le champ "name_edit_intitule_type_html"
            return render_template('exercice/exercice_edit.html', data=valeur_edit_list)

    return render_template("exercice/exercice_update.html")
# Select delete
@obj_mon_application.route('/exercice_select_delete', methods=['POST','GET'])
def exercice_select_delete():

    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_exercice = GestionExercice()
            # OM 2019.04.04 Récupérer la valeur de "idGenreDeleteHTML" du formulaire html "GenresDelete.html"
            id_exercice_delete = request.args.get('id_exercice_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_exercice": id_exercice_delete}


            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_exercice = obj_actions_exercice.delete_select_exercice_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur genres_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('exercice/exercice_delete.html', data = data_id_exercice)
# Delete exercice
@obj_mon_application.route('/exercice_delete', methods=['POST','GET'])
def exercice_delete():
    if request.method == 'POST':
        try:
            obj_actions_exercice = GestionExercice()
            id_exercice_delete = request.form['id_exercice_delete_html']
            valeur_delete_dictionnaire = {"value_id_exercice": id_exercice_delete}
            data_exercice = obj_actions_exercice.delete_exercice_data(valeur_delete_dictionnaire)
            return redirect(url_for('exercice_afficher'))
        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # Cette erreur 1451 signifie qu'on veut effacer un exercice qui est associé à un type dans la table
            # intermédiaire t_avoirtype
            if erreur.args[0] == 1451:
                flash('Impossible d\'effacer cet exercice car il est associé à un type d\'exercice')
                print(f"Impossible d'effacer !!!! Cet exercice est associé à un/des type(s) dans la table intermédiaire : {erreur}")
                return redirect(url_for('exercice_afficher'))
            else:
                print(f"Erreur exercice_delete {erreur.args[0], erreur.args[1]}")
                flash(f"Erreur exercice_delete {erreur.args[0], erreur.args[1]}")
    return render_template('exercice/exercice:afficher.html', data=data_exercice)