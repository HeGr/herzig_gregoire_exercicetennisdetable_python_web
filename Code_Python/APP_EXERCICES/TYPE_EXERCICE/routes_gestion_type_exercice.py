# routes_gestion_type_exercice.py
# GH 2020.04.22 Gestion des routes flask pour la table intermédiaire qui associe les exercices et les types
from flask import render_template
from Code_Python.APP_EXERCICES import obj_mon_application

#-------------------------------------------------------------------------------------------------------
# Définition d'une route pour /type_exercice_afficher.html
# cela va permettre de programmer els actions avant d'intéragir
# avec le navigateur par la méthode render_template
# Pour tester http://127.0.0.1/type_exercice_afficher
#-------------------------------------------------------------------------------------------------------
@obj_mon_application.route("/type_exercice_afficher", methods=['GET','POST'])
def type_exercice_afficher():
    # # savoir si les données d'un formulaire sont un affichage
    # # ou un envoi de données par des champs du formulaire HMTL
    # if request.method == "GET":
    #     try:
    #         # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
    #         obj_actions_type_exercice = GestionTypeExercice()
    #         # Récupére les données grâce à une requête MySql définie dans la classe GestionTypeExercice()
    #         # Fichier data_gestion_type.py
    #         data_type_exercice = obj_actions_type_exercice.type_afficher_data()
    #         # DEBUG bon marché : Pour afficher un message dans la console.
    #
    #         # OM 2020.04.09 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
    #         flash("Données type d'exercice affichées !!", "Success")
    #     except Exception as erreur:
    #         print(f"RGFG Erreur générale.")
    #         # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
    #         # Ainsi on peut avoir un message d'erreur personnalisé.
    #         # flash(f"RGG Exception {erreur}")
    #         raise Exception(f"RGFG Erreur générale. {erreur}")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("type_exercice/type_exercice_afficher.html")
