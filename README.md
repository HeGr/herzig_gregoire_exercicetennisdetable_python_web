# Herzig_Gregoire_ExerciceTennisTable_PYTHON_WEB
***
> Wiki

Vous trouverez toute la documentation utile dans mon wiki :

https://gitlab.com/HeGr/herzig_gregoire_exercicetennistable_python_web/-/wikis/Accueil

**MAIS** si vous avez trop la flemme d'aller cliquer sur le Wiki, vous trouverez les lignes de commandes (sans explications) ci dessous

***
    `cd <path>`

    `mkdir <mon_dossier>`
    
    `cd <path du dossier>`

    `git clone https://gitlab.com/HeGr/herzig_gregoire_exercicetennistable_python_web.git`

    `git status`

    `git commit -a -m "description du commit" `

    `git push` ou `git push -u origin <branch name>`

